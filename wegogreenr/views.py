from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden, HttpResponse

# Create your views here.

def HomeView(request):
    return render(request, "home.html", context={})

def ListingView(request):
    return render(request, "listings.html", context={})

def HouseDetailsView(request):
    return render(request, "house-details.html", context={})

def ExperienceDetailsView(request):
    return render(request, "experience-details.html", context={}) 

def ContactView(request):
    return render(request, "contact.html", {'site_key': settings.RECAPTCHA_SITE_KEY})            
