from django.db import models


class User(models.Model):
    pass

class Listing(models.Model):
    """
    A Listing is a housing that is available for rent on WGG
    """
    title = models.CharField(max_length=255)
    description = models.TextField()
    host = models.ForeignKey(to=User, on_delete=models.CASCADE)

class Booking(models.Model):
    """
    """
    host = models.ForeignKey(to=User, on_delete=models.CASCADE)
    guest = models.ForeignKey(to=User, on_delete=models.CASCADE)
    from_date = models.DateField()
    to_date = models.DateField()
    price = models.PositiveSmallIntegerField(
        help_text='Price in €'
    )
